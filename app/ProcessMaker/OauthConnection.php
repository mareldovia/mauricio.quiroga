<?php

namespace App\ProcessMaker;

use Exception;
use Cache;
use App\ProcessMaker\ProcessMaker;

class OauthConnection{

	public static function  getRealUsers(){
		$oProcessMaker=new ProcessMaker(); // getting access token
		$apiServer=env('PROCESSMAKER_URL');
		$ch = curl_init($apiServer."/api/1.0/workflow/users");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer " . $oProcessMaker->getAccessToken()));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$aUsers = json_decode(curl_exec($ch));
		return $aUsers;
	}


	/**
	* this connection function will approve actual user configured on .env file
	*/
	public static function approveUser(){
		$oProcessMaker=new ProcessMaker(); // getting access token
		$postParams=array(
			'usr_username'=>env('PROCESSMAKER_USER'),
			'usr_password'=>env('PROCESSMAKER_PASSWORD')
		);
		$apiServer=env('PROCESSMAKER_URL');
		$ch=curl_init($apiServer."/api/1.0/workflow/plugin-test1/pass");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer " . $oProcessMaker->getAccessToken()));
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postParams);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = json_decode(curl_exec($ch),true);
		return $response;
	}

}