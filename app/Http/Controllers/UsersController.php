<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Faker\Generator;
use App\ProcessMaker\OauthConnection;

class UsersController extends Controller
{
	public function index(Request $request, Generator $faker) {
		$list = [];
		for($i=0; $i < 100; $i++) {
			$list[]=[
		        "usr_username" => $faker->name,
		        "usr_email" => $faker->unique()->safeEmail,
		        "usr_create_date" => $faker->date,
		    ];
	    }
	    return ["data" => $list];
	}
	/*
	* Get data from .env configured server. 
	*/
	public function realData(){
		$aUsers=OauthConnection::getRealUsers(); // get users from server configured on .env file.
		$list = [];
		foreach ($aUsers as $oUser ) {
			$list[]=[
		        "usr_username" => $oUser->usr_firstname.' '.$oUser->usr_lastname,
		        "usr_email" => $oUser->usr_email,
		        "usr_create_date" => $oUser->usr_create_date,
		    ];
	    }
	    return ["data" => $list];
	}

	public function approve() {
		$response=OauthConnection::approveUser(); //this connection function will approve actual user configured on .env file		
		return $response;
	}
}
